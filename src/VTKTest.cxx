#include "vtkNIFTIImageReader.h"
#include "vtkNIFTIImageHeader.h"

#include "vtkRenderWindowInteractor.h"
#include "vtkInteractorStyleImage.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"
#include "vtkCamera.h"
#include "vtkMatrix4x4.h"
#include "vtkImageData.h"
#include "vtkImageReslice.h"
#include "vtkImageSliceMapper.h"
#include "vtkImageProperty.h"
#include "vtkImageSlice.h"
#include "vtkImageReader2.h"
#include "vtkSmartPointer.h"
#include "vtkStringArray.h"
#include "vtkErrorCode.h"

#define VTK_CREATE(type, name) \
  vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

int main(int argc, char *argv[])
{
  // VTK pipeline initialization
  VTK_CREATE(vtkRenderWindowInteractor, iren);
  VTK_CREATE(vtkInteractorStyleImage, style);
  style->SetInteractionModeToImage3D();
  VTK_CREATE(vtkRenderWindow, renWin);
  iren->SetRenderWindow(renWin);
  iren->SetInteractorStyle(style);

  const char *filename = 0;
  if (argc > 1) {
    filename = argv[1];
  }

  VTK_CREATE(vtkNIFTIImageReader, reader);
  reader->SetFileName(filename);
  reader->SetTimeAsVector(true);
  if (reader->GetTimeAsVector()) cout << "time as vector" << endl;
  //cout << reader->GetTimeDimension() << endl;
  //cout << reader->GetTimeSpacing() << endl;
  reader->Update();

  vtkSmartPointer<vtkNIFTIImageHeader> header = reader->GetNIFTIHeader();
  header->Print(cout);

  if (reader->GetErrorCode() != vtkErrorCode::NoError) {
    return 1;
  }

  VTK_CREATE(vtkMatrix4x4, matrix);
  if (reader->GetQFormMatrix()) {
    matrix->DeepCopy(reader->GetQFormMatrix());
    matrix->Invert();
    cout << "QForm" << endl;
  }
  else if (reader->GetSFormMatrix()) {
    matrix->DeepCopy(reader->GetSFormMatrix());
    matrix->Invert();
    cout << "SForm" << endl;
  }

  VTK_CREATE(vtkImageReslice, reslice);
  reslice->SetInputConnection(reader->GetOutputPort());
  reslice->SetResliceAxes(matrix);
  reslice->SetInterpolationModeToLinear();
  reslice->Update();

  double range[2];
  int extent[6];
  reslice->GetOutput()->GetScalarRange(range);
  reslice->GetOutput()->GetExtent(extent);

  for (int j = 0; j < 2; j++)
  {
    cout << "range " << range[j] << endl;
  }
  for (int j = 0; j < 6; j++)
  {
    cout << j << ": " << extent[j] << endl;
  }
  static double viewport[3][4] = {
    { 0.67, 0.0, 1.0, 0.5 },
    { 0.67, 0.5, 1.0, 1.0 },
    { 0.0, 0.0, 0.67, 1.0 },
  };

  // check if image is 2D
  bool imageIs3D = (extent[5] > extent[4]);

  for (int i = 2 * (imageIs3D == 0); i < 3; i++) {
    VTK_CREATE(vtkImageSliceMapper, imageMapper);
    if (i < 3) {
      imageMapper->SetInputConnection(reslice->GetOutputPort());
    }
    imageMapper->SetOrientation(i % 3);
    imageMapper->SliceAtFocalPointOn();

    VTK_CREATE(vtkImageSlice, image);
    image->SetMapper(imageMapper);

    image->GetProperty()->SetColorWindow(range[1] - range[0]);
    image->GetProperty()->SetColorLevel(0.5*(range[0] + range[1]));
    image->GetProperty()->SetInterpolationTypeToNearest();

    VTK_CREATE(vtkRenderer, renderer);
    renderer->AddViewProp(image);
    renderer->SetBackground(0.0, 0.0, 0.0);
    if (imageIs3D) {
      cout << "Image is 3D" << endl;
      renderer->SetViewport(viewport[i]);
    }

    renWin->AddRenderer(renderer);

    // use center point to set camera
    double *bounds = imageMapper->GetBounds();
    double point[3];
    point[0] = 0.5*(bounds[0] + bounds[1]);
    point[1] = 0.5*(bounds[2] + bounds[3]);
    point[2] = 0.5*(bounds[4] + bounds[5]);
    double maxdim = 0.0;
    for (int j = 0; j < 3; j++) {
      double s = 0.5*(bounds[2 * j + 1] - bounds[2 * j]);
      maxdim = (s > maxdim ? s : maxdim);
    }

    vtkCamera *camera = renderer->GetActiveCamera();
    camera->SetFocalPoint(point);
    if (imageMapper->GetOrientation() == 2) {
      point[imageMapper->GetOrientation()] -= 500.0;
      camera->SetViewUp(0.0, +1.0, 0.0);
    }
    else {
      point[imageMapper->GetOrientation()] += 500.0;
      camera->SetViewUp(0.0, 0.0, +1.0);
    }
    camera->SetPosition(point);
    camera->ParallelProjectionOn();
    camera->SetParallelScale(maxdim);
  }

  if (imageIs3D) {
    renWin->SetSize(600, 400);
  }
  else {
    renWin->SetSize(400, 400);
  }

  renWin->Render();
  iren->Start();

  // code for generating a regression image
  //int retVal = vtkRegressionTestImage( renWin );
  //if ( retVal == vtkRegressionTester::DO_INTERACTOR ) {
  //  iren->Start();
  //}
  // return !retVal;

  return 0;
}